# Fonte de Tensão Ajustável - Projeto de Eletrônica

## Alunos

| Nome             | Número USP   |
| -----------------|:------------:|
| Felipe Gandra Moreira   | 14569550     |
| Allan Vitor de Souza Silva      | 14712657     |
| Artur Oliveira Arraes     | 14745532     |
| Matheus Aparecido de Almeida Rodriguez | 14597868     |

## Lista de componentes

Lista-se, a seguir, todos os componentes utilizados no projeto.

1. **Protoboard** - Placa de ensaio de circuitos elétricos que nos permite testar nosso projeto com facilidade.
2. **Fios** - Componentes capaz de conduzir corrente elétrica e unir diferentes partes do circuito.
3. **Resistores** - Componentes responsáveis por limitar a corrente elétrica.
     - 100Ω
     - 120Ω
     - 2k2Ω
     - 820Ω
4. **Diodos** - Dispositivos capaz de interromper a passagem de corrente em um sentido. Nos ajuda a transformar corrente alternada em contínua.
5. **Led** - Componente que emite luz caso seja percorrido com corrente elétrica no sentido correto.
6. **Capacitor** - São dispositivos capazez de armazenar energia. É extremamente importante pois auxilia na filtragem, reduzindo a variação de tensão.
     - 470uF
7. **Transistor** - Dispositivo responsável por auxiliar na regulação da tensão.
8. **Potenciômetro** - Componente com uma resistência variável, é responsável por controlar a tensão atuante na carga do nosso projeto.
     - 5kΩ
9. **Transformador 25V** - Componente capaz de modificar a tensão por meio de indução eletromagnética. É fundamental no projeto pois reduz bruscamente a tensão da tomada.



## Preço dos componentes

Aqui estão listados todos os componentes que precisaram ser comprados. Percebe-se que não são todos os componentes do projeto, visto que o grupo já possuia alguns.

| Componente        | Quantidade    | Preço (R$) |
| ------------------|:-------------:| ----------:|
| Resistor 120Ω     | 10            |  0,7       |
| Resistor 2k2Ω     | 10            |   0,7      |
| Potenciômetro 5kΩ | 2             |    14,00   |
| Diodo             | 10            |    2       |
| Diodo Zener 13V   | 1             |    0,5     |
| Capacitor 470uF   | 1             |    2,8     |
| LED Vermelho      | 1             |    0,5     |
| Transistor BC369  | 1             |    0,5     |
| Transistor BC337  | 1             |    0,5     |
| Resistor 100Ω 2W  | 1             |    1,2     |
| Resistor 150Ω 2W  | 1             |    1,2     |
| Resistor 100Ω 2W  | 1             |    0,5     |
| -                 |  -            |    25,1    |


## Imagem do circuito no Falstad

Segue a imagem do circuito projetado no falstad.

[Link do circuito](https://tinyurl.com/2z6ao2l7)

![Circuito no falstad](https://gitlab.com/Allanvtr/fonte-de-tensao-ajustavel/-/raw/main/imagens/Screenshot_from_2024-06-24_16-41-51.png)


## Imagem do projeto no EAGLE

![Circuito no Eagle](https://gitlab.com/Allanvtr/fonte-de-tensao-ajustavel/-/raw/main/imagens/WhatsApp_Image_2024-06-16_at_22.38.56.jpeg)


## Imagem dos Cálculos

 ![Calculos](https://gitlab.com/Allanvtr/fonte-de-tensao-ajustavel/-/raw/main/imagens/calculocorrente.jpg?ref_type=heads)

## Vídeo no Youtube explicando como foi feito

(https://youtu.be/F7vv5hyqRZs?si=FtSQ3fmpQSTYvvUh)

obs: No vídeo há um pequeno erro de cálculo causado pelo uso do valor de corrente incorreto. Esse erro é corrigido no papel com os cálculos.
